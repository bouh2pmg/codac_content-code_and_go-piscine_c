#/bin/bash

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#%%%
#%%% EPITECH CONTEX template compilation script
#%%%
#%%%                Pierre ROBERT
#%%%                January 2016
#%%%
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



# BINARIES CHECKING
if ! which pdflatex > /dev/null; then
	echo -e "pdflatex not found !"
	echo -e "try 'sudo apt-get install texlive-full' to install it"
	exit
fi


# ARGUMENTS CHECKING
USAGE="\nUSAGE : $0 [-l logo] source.tex\n\n"
USAGE+="\t-l logo (def:none) \tpresentation page logo (150p height)\n"
USAGE+="\tsource.tex      \ttex file describing the document to be compiled\n"
LOGO=-1
while getopts l: name
do
	case $name in
        	l)LOGO=$OPTARG;;
			*)echo -e "$USAGE"; exit;;
        esac
done

shift $((OPTIND-1))

if [ $# -ne 1 ]; then
	echo -e "$USAGE"
	exit
fi

SOURCE_FILE="$1"
if [ ! -e $SOURCE_FILE ]; then
	SOURCE_FILE="$1.tex"
fi
if [ ! -e $SOURCE_FILE ]; then
	SOURCE_FILE="$1tex"
fi
if [ ! -e $SOURCE_FILE ]; then
	echo -e "\nSource $1 not found...\n"
	exit
fi

SOURCE_DIR=${SOURCE_FILE%.tex}
SOURCE_DIR=${SOURCE_DIR%/*}
OUTPUT_DIR="./PDF/"
PDF_NAME=${SOURCE_FILE%.tex}
PDF_NAME=${PDF_NAME##*/}
SUBJECT=subject.temp
TEMPLATE_DIR="./templateLatex_coding"
IMG_DIR="$TEMPLATE_DIR/img"
MAIN_FILE="Epitech_CONTEX_template"

if [ -e $LOGO ]; then
	echo -e "Logo file $LOGO found...\n"
elif [ `ls $SOURCE_DIR/logo*.png $SOURCE_DIR/logo*.jpg 2>/dev/null | wc -w` -ge 1 ]; then
	LOGO=`ls -x $SOURCE_DIR/logo*.png $SOURCE_DIR/logo*.jpg 2>/dev/null | cut -d" " -f1`
	echo -e "Logo file $LOGO found...\n"
else
	echo -e "\n/!\ No logo file found...\n"
	LOGO="$IMG_DIR/pixelTransparent.png"
fi


# VARIABLES CHECKING
function chekIfRedefined
{
	grep "^\\\renewcommand{\\\\$1" $SOURCE_FILE > /dev/null
	if [ $? -eq 1 ]; then
		echo -e "\nThe $1 variable must be redefined in $SOURCE_FILE...\n"
		exit
	else
		echo -e "$1 : \t\tOK"	
	fi
}

# OBOSLETE VARIABLES CHECKING
function chekObsolete
{
	grep "^\\\renewcommand{\\\\$1" $SOURCE_FILE > /dev/null
	if [ $? -ne 1 ]; then
		echo -e "\nThe $1 variable should not be redefined anymore in $SOURCE_FILE...\n"
		echo -e "(enter to continue)"
#		read a
	fi
}

echo -e "Subject : ${PDF_NAME}"
echo -e "\n-----------CHECKING REDEFINED VARIABLES------------"
chekIfRedefined "moduleCode"
chekIfRedefined "subjectTitle"
chekIfRedefined "binaryName"
chekIfRedefined "subjectSubTitle"
chekIfRedefined "repository"
chekIfRedefined "devLanguage"
chekIfRedefined "groupSize"
chekObsolete "moduleName"
chekObsolete "semester"



# COMPILATION
echo -e "\n-----------COMPILATION------------"
echo -e "----------------------------------"
echo -e "[COMPILATION] pdf generation..."
rm -f ${PDF_NAME}.pdf
cp ${SOURCE_FILE} "$TEMPLATE_DIR/$SUBJECT.tex"
cp $LOGO "$IMG_DIR/logo_custom.png"
cd $TEMPLATE_DIR && pdflatex -halt-on-error -file-line-error -draftmode --jobname=${PDF_NAME} ${MAIN_FILE}.tex && pdflatex -halt-on-error -file-line-error --jobname=${PDF_NAME} ${MAIN_FILE}.tex
cd - > /dev/null
if [ -e "$TEMPLATE_DIR/$PDF_NAME.pdf" ]; then
	mv "$TEMPLATE_DIR/$PDF_NAME.pdf" "$OUTPUT_DIR"
fi


# NETTOYAGE
echo -e "----------------------------------"
echo -e "[COMPILATION] cleaning...\n"
rm -f "$TEMPLATE_DIR/$SUBJECT.tex" \
	"$TEMPLATE_DIR/$PDF_NAME.aux" \
	"$TEMPLATE_DIR/subject.temp.aux" \
	"$TEMPLATE_DIR/Epitech_CONTEX_modules-definition.aux" \
	"$TEMPLATE_DIR/$PDF_NAME.log" \
	"$TEMPLATE_DIR/$PDF_NAME.out" \
	"$TEMPLATE_DIR/$PDF_NAME.sol"
