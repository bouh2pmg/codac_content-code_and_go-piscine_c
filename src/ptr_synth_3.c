void my_swap(int *a, int *b)
{
    int tmp;

    tmp = *a;
    *a = *b;
    *b = tmp;
}

int main()
{
    int a, b;

    a = 10;
    b = -10;
    printf("a: %d, b: %d\n", a, b); // 10, -10
    my_swap(a, b);
    printf("a: %d, b: %d\n", a, b); // -10, 10
    return 0;
}
