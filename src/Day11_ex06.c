//...
#include <stdlib.h>
#define	EMPTY 0
#define BLOCKED 1
//...
void	verif_return(int *ret)
{
  if (ret != NULL)
    printf("line :\t%d\nColumn :\t%d\n", ret[0], ret[1]);
  else
    printf("Nothing found in the given range.\n");
}

int	main()
{
  //...
  int	lines[4];
  int	columns[4];
  //...
  lines[0] = BLOCKED;
  lines[1] = BLOCKED;
  lines[2] = EMPTY;
  lines[3] = BLOCKED;
  columns[0] = EMPTY;
  columns[1] = EMPTY;
  columns[2] = BLOCKED;
  columns[3] = BLOCKED;
  verif_return(look_for_value(table, lines, columns, 2));
  verif_return(look_for_value(table, lines, columns, 1));
  //...
}
