void pass_value_to_ptr(int* p, int val)
{
    *p = val;
}

int main()
{
    int i = 0;
    printf("i: %d\n", i); // 0
    pass_value_to_ptr(&i, 10);
    printf("i: %d\n", i); // 10
    return 0;
}
