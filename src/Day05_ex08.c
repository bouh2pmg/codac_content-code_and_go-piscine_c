int	my_putnbr_base(int nbr, char *base);
int	main()
{
  my_putnbr_base(42, "0123456789");
  my_putnbr_base(42, "0123456789ABCDEF");
  my_putnbr_base(42, "9876543210");
  my_putnbr_base(42, "+-*/%()-_=");
}
