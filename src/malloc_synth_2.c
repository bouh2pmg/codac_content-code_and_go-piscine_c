int *int_arr;
int nb_of_ints = 10;
// then we could do:
// int_arr = malloc(nb_of_ints)
// BUT ints are 4 bytes large
// so we could solve the problem this way
// int_arr = malloc(nb_of_ints * 4)
// it works but this " * 4" doesn't seem to come from anywhere
// the solution lies in the use of "sizeof"
// int_arr = malloc(nb_of_ints * sizeof(int))
// sure but why int? well it's the type of the array. Ok, isn't there a way to make it clearer?
// yes there is:
int_arr = malloc(nb_of_ints * sizeof(*int_arr))
// meaning we allocate nb_of_ints times the size of the type corresponding to an element of 
// the array thanks the *int_arr which dereference the pointer and returns the type of the variable at the
// pointers address which here is an int a which size is 4.
