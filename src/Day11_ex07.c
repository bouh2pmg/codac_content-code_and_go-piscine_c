int	main()
{
  int	**table;
  //...
  int	*ret_value;
  int	*ret_space;
  int	lines[4];
  int	columns[4];
  //...
  lines[0] = BLOCKED;
  lines[1] = EMPTY;
  lines[2] = BLOCKED;
  lines[3] = BLOCKED;
  columns[0] = EMPTY;
  columns[1] = EMPTY;
  columns[2] = EMPTY;
  columns[3] = EMPTY;

  ret_value = look_for_value(table, lines, columns, 0);

  lines[0] = EMPTY;
  lines[1] = BLOCKED;

  ret_space = look_for_space(table, lines, columns, 0);

  rotate_lines(table, ret_value[0], ret_value[1] - ret_space[1]);
  print_tab(table);
  rotate_columns(table, ret_space[1], ret_value[0] - ret_space[0]);
  print_tab(table);
  //...
}
