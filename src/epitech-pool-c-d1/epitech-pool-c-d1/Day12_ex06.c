#include <stdio.h>

int	matrices_addition(int **, int column_offset, int row_offset,	\
			  int size, int direction);

int	main()
{
  int	mat[5][5] =
    {
      {0, 1, 2, 3, 4},
      {5, 6, 7, 8, 9},
      {10, 11, 12, 13, 14},
      {15, 16, 17, 18, 19},
      {20, 21, 22, 23, 24},
    };
  int	*tab[5];

  tab[0] = mat[0];
  tab[1] = mat[1];
  tab[2] = mat[2];
  tab[3] = mat[3];
  tab[4] = mat[4];
  printf("%d\n", matrices_addition(tab, 0, 0, 5, 3));
  printf("%d\n", matrices_addition(tab, 0, 0, 5, 6));
  printf("%d\n", matrices_addition(tab, 0, 0, 5, 0));
  printf("%d\n", matrices_addition(tab, 3, 3, 5, 11));
  printf("%d\n", matrices_addition(tab, 3, 3, 5, -2));
  printf("%d\n", matrices_addition(tab, 3, 22, 5, 2));
  return (0);
}
