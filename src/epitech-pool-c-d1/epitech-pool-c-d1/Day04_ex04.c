#include <stdio.h>

void	my_sort_in_tab(int *, int);
int	main()
{
  int	tab[5] = {8, 7, 6, 5, 4};
  
  my_sort_in_tab(tab, 5);
  printf("%d %d %d %d %d\n", tab[0], tab[1], tab[2], tab[3], tab[4]);
  return (0);
}
