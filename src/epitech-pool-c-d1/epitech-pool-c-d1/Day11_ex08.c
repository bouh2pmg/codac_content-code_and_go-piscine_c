#define PRINT_SQUARE_DEBUG__ 1
// ...
void	build_first_square(int **table)
{
  print_tab(table)
  build_first_line(table);
  line_to_square(table, 0);
}
