#include <stdio.h>

void	my_init(int *);
int	main()
{
  int	i;

  i = 0;
  my_init(&i);
  printf("%d\n", i);
}
