#include <stdlib.h>

void	my_putstr(char *);
char	*my_concat_str(char **);
int	main()
{
  char	*tab[] = {
    "Hello",
    "Students\n",
    NULL
  };
  char	*str;

  str = my_concat_str(tab);
  my_putstr(str);
  return (0);
}
