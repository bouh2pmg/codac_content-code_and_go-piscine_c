#include <string.h>

void	my_reset_ptr(char **ptr);
int	main()
{
  char	*str;

  str = strdup("Please, let me free !");
  my_reset_ptr(&str);
  return (0);
}
