int main()
{
    char *ptr = "toto\0";
    printf("ptr addr: %p\n", ptr);
    // the address points to the first character of the string you
    // can verify it this way
    printf("ptr addr: %p\n", &ptr[0]); // address of this first index of ptr
    return 0;
}
